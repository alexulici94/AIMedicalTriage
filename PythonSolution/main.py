import subprocess

def test(os):
    p = subprocess.Popen(['python', 'algo.py', '--action', 'prediction'], stdin = subprocess.PIPE, stdout = subprocess.PIPE)
    
    osStr = ""
    if(len(os) > 0):
        osStr = os[0]
        for i in range(1,len(os)):
            osStr = osStr + "\n" + os[i]
 
    out, _ = p.communicate(osStr)
    print out
    
    
def testSymptoms():
    p = subprocess.Popen(['python', 'algo.py', '--action', 'symptoms'], stdin = subprocess.PIPE, stdout = subprocess.PIPE)
    out, _ = p.communicate()
    print out
    

testSymptoms()

test(["dispnee", "durere localizare precordiala", "coma"])
test(["dispnee", "durere localizare precordiala", "aritmie", "ahc - ima", "anxietate"])
test(["dezhidratare", "dispnee", "vertij", "sincopa", "cefalee"])
test(["cmv - consum droguri", "cefalee", "greata", "febra", "hipotensiune"])
test(["tumefactie", "scurtarea membrului superior", "app - sedentarism"])
test(["tumefactie", "scurtarea membrului superior", "app - sedentarism", "durere"])
test(["scurtarea membrului superior", "app - sedentarism", "durere"])
test(["leziune eritematoasa", "flictene", "ameteli"])
test(["alterarea starii de constienta", "ameteli", "dispnee"])
test(["alterarea starii de constienta", "ameteli", "dispnee", "cmv - risc intoxicatie cu co"])
test(["febra", "disurie", "polakiurie"])
test(["febra", "disurie", "polakiurie", "varsaturi", "durere localizare lombara"])
test(["hemoragii cutanate", "anemie", "tahicardie", "hipotensiune"])
test(["hemoragii cutanate", "anemie", "tahicardie", "hipotensiune","echimoze"])
test(["febra","disurie", "polakiurie","durere localizare hipogastru"])
test
