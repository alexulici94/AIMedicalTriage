﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using TriageAPI.Repository;

namespace TriageAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class PatientController : ApiController
    {
        public List<Patient> Patients;
        const string getSymptomsArg = "--action prediction";

        // POST api/values
        public JsonResult<Result> Post([FromBody]Patient patient)
        {
            Database db = new Database();
            //db.AddToDB("123", "Hello", Constants.PatientsTable);
            //savePatientToDb
            Executable runPython = new Executable();
            Mapper map = new Mapper();

            var output = runPython.RunExe(Constants.PythonAlgPath, getSymptomsArg, map.SymptomsToStrArray(patient.Symptoms.ToList()));
            //return Json(response);
            //apply algorithm
            List<Disease> disease = new List<Disease>();
            float max = 0;
            foreach(var x in output)
            {
                var split = x.Split(';');
                if (!string.IsNullOrEmpty(x))
                {
                    Disease ab = new Disease
                    {
                        Name = split[0],
                        Probability = Convert.ToSingle(split[1]),
                        Score = Convert.ToSingle(split[2])
                    };

                    if (max < ab.Score)
                        max = ab.Score;

                    disease.Add(ab);
                }
            }

            disease = disease.OrderByDescending(x => x.Score).Take(3).ToList();

            //List<Result> asdv = new List<Result>();
            var result = new Result
            {
                Score = max.ToString(),
                Diseases = disease,
                PatientId = Guid.NewGuid().ToString(),
                PatientName = patient.Name
            };
            //asdv.Add(result);
            return Json(result);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]Patient patient)
        {
            //apply algorithm
        }
    }
}
