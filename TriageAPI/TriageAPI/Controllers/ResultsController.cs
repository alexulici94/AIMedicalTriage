﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TriageAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ResultsController : ApiController
    {

        // GET: api/Results
        public IEnumerable<string> Get()
        {

            return null;
        }

        // GET: api/Results/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Results
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Results/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Results/5
        public void Delete(int id)
        {
        }
    }
}
