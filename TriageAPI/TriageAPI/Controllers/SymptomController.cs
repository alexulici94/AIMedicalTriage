﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace TriageAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class SymptomController : ApiController
    {
        const string getSymptomsArg = "--action symptoms";

        // GET: api/Symptom
        public JsonResult<IEnumerable<Symptom>> Get()
        {
            Executable runPython = new Executable();
            Mapper map = new Mapper();

            var output = runPython.RunExe(Constants.PythonAlgPath, getSymptomsArg);
            var response = map.StringToStringArray(output);
            return Json(response);
        }
    }
}
