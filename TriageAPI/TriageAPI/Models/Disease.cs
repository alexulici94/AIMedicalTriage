﻿public class Disease
{
    public string Id { get; }
    public string Name { get; set; }
    public float Probability { get; set; }
    public float Score { get; set; }
}