﻿using System.Collections.Generic;

public class Patient
{
    public string Id { get; set; }
    public string Name { get; set; }
    public IEnumerable<Symptom> Symptoms { get; set; }
}