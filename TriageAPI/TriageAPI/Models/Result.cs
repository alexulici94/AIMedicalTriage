﻿using System.Collections.Generic;

public class Result
{
    public string PatientId { get; set; }
    public string PatientName { get; set; }
    public string Score { get; set; }
    public List<Disease> Diseases { get; set; }
}