﻿using STSdb4.Data;
using STSdb4.Database;
using System.Collections.Generic;

namespace TriageAPI.Repository
{
    public class Database
    {
        private IStorageEngine engine;

        public ITable<Data<string>, Data<string>> OpenTable(string tableName)
        {
            engine = STSdb.FromFile(Constants.DatabaseName);
            return engine.OpenXTable<Data<string>, Data<string>>(tableName);
        }

        public void AddToDB(string key, string value, string tableName)
        {
            var table = OpenTable(tableName);
            table[new Data<string>(key)] = new Data<string>(value);
            engine.Commit();
        }

        public Dictionary<string, string> LoadFromDB(string tableName)
        {
            var table = OpenTable(tableName);
            return TableToDictionary(table);
        }

        private Dictionary<string, string> TableToDictionary(ITable<Data<string>, Data<string>> table)
        {
            var result = new Dictionary<string, string>();
            foreach (var row in table)
            {
                result.Add(row.Key.Value, row.Value.Value);
            }
            return result;
        }
    }
}