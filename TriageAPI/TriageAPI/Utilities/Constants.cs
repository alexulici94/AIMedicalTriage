﻿public class Constants
{
    public const string DatabaseName = "db";
    public const string PatientsTable = "patients";
    public const string PythonAlgPath = @"C:\Data\Projects\MedHack\TriageAPI\TriageAPI\bin\python\dist\algo\algo.exe";
}