﻿using System;
using System.Diagnostics;

public class Executable
{
    public string[] RunExe(string filePath, string arguments, string[] input = null)
    {
        // Start the child process.
        Process p = new Process();
        // Redirect the output stream of the child process.
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        if (input!=null)
        {
            p.StartInfo.RedirectStandardInput = true;
        }
        p.StartInfo.CreateNoWindow = true;
        p.StartInfo.Arguments = arguments;
        p.StartInfo.FileName = Constants.PythonAlgPath;
        p.Start();
        // Do not wait for the child process to exit before
        // reading to the end of its redirected stream.
        // p.WaitForExit();
        // Read the output stream first and then wait.
        if (input!=null)
        {
            foreach(var a in input)
                p.StandardInput.WriteLine(a);
            p.StandardInput.Close();
        }
        string output = p.StandardOutput.ReadToEnd();
        p.WaitForExit();
        var res = output.Split(new string[] { "\r\n" }, StringSplitOptions.None);
        return res;
    }
}