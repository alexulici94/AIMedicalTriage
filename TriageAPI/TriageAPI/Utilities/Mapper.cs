﻿using System.Collections.Generic;

public class Mapper
{
    public IEnumerable<Symptom> StringToStringArray(string[] str)
    {
        List<Symptom> result = new List<Symptom>();
        int count = 0;
        foreach(string symptomName in str)
        {
            Symptom newSymptom = new Symptom
            {
                Id = count.ToString(),
                Name = symptomName
            };
            count++;
            result.Add(newSymptom);
        }
        return result;
    }

    public string[] SymptomsToStrArray(List<Symptom> symptoms)
    {
        string[] result = new string[symptoms.Count];
        int count = 0;
        foreach (Symptom symp in symptoms)
        {
            result[count] = symp.Name;
            count++;
        }
        return result;
    }
}