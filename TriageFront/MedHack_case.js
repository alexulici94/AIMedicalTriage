var myAcc,
	dhxForm
var aChosenSimptoms = [];
var activeAccCellId;
var activeAccCellName;

var dataGrid = {"total_count":50000, "pos":0, "data":[
{ "name": "A Time to Kill",
"probability": "John Grisham"
},
{ "name": "Blood and Smoke",
"probability": "Stephen King"
},
{ "diagnostic": "The Rainmaker",
"probability": "John Grisham"
}
]};
function doOnLoad() {

	function savePatient(cName, aChosenSimptoms) {
		var toSend = {
			'Id': 0,
			'Name': cName,
			'Symptoms': aChosenSimptoms
		};

		$.ajax({
            url: 'http://localhost:50681/api/patient',
            dataType: 'json',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: toSend,
            success: function( data, textStatus, jQxhr ){
            	console.log(data);
                console.log('Successfuly saved!');
                refresh(data);

            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
	}

	function takePatient() {

	}

	function removePatient() {

	}

	var myLayout = new dhtmlXLayoutObject({
        parent: document.body,  // parent container
        pattern: "2U"           // layout's pattern
    });
    myLayout.cells("a").hideHeader();
    myLayout.cells("b").hideHeader();
    refreshPatients();


    //LAYOUT LEFT HANDLING

	//REFRESH REFRESH

	window.setInterval(refreshPatients, 5000);

	// function refreshPatients() { 

		function refresh(data) {

			var dataCases = data;
			myLayout.cells("a").progressOn();
			if (myAcc != null) {
				myAcc.unload();
				myAcc = null;
			}
			var items = [];
			dataCases.forEach(function(entry) {
			    items.push({id: entry.PatientId, text: entry.PatientName + "<span style='float:right'>" + entry.Score + "</span>", icon: "flag_red.png"});
			});

		    myAcc = myLayout.cells("a").attachAccordion({
				icons_path: "dhtmlxSuite_v51_std/samples/dhtmlxAccordion/common/icons/",
				items: items
			});

			myAcc.attachEvent("onActive", function(id){
				activeAccCellId = id;
			});

			dataCases.forEach(function(entry) {
				var myToolbar = myAcc.cells(entry.PatientId).attachToolbar({
					align: 'left'
				});
				myToolbar.setIconset("awesome");
				myToolbar.addButton("Take", 2, "<i class='fa fa-check' style='margin-right:7px'></i><span style='color:red'>Take</span>", "", "open_dis.gif");
				myToolbar.addButton("Edit", 3, "<i class='fa fa-edit' style='margin-right:7px'></i><span style='color:blue'>Edit</span>", "open.gif", "open_dis.gif");
				myToolbar.addButton("Remove", 4, "<i class='fa fa-trash' style='margin-right:7px'></i><span style='color:blue'>Remove</span>", "open.gif", "open_dis.gif");

				myToolbar.attachEvent("onClick", function(id) {
					if (id == 'Take') {
						takePatient();
					}
				});

				var myGrid = myAcc.cells(entry.PatientId).attachGrid();
				myGrid.setColumnIds("Name,Probability");
				myGrid.setHeader("Diagnostic, Probability");
				myGrid.setColAlign("left,left");
				myGrid.setInitWidths("*,*");
				myGrid.setColTypes("ro,ro");
				myGrid.init();
				myGrid.parse(entry.Diseases,"js");
			});

			if (activeAccCellId) 
				myAcc.cells(activeAccCellId).open();

			myLayout.cells("a").progressOff();
		// }


		// $.ajax({
		//   type: "GET",
		//   url: "http://localhost:50681/api/results",
		//   cache: false,
		//   success: function(data) {
		//   	refresh(data);
		//  //     $.each(data, function (i, option) {
		// 	//     $(item.lastChild).append($('<option>', { 
		// 	//         value: option._id,
		// 	//         text : option.name 
		// 	//     }));
		// 	// });
		//   }
		// });
		
	}




	//LAYOUT RIGHT HANDLING

	var formStructure = [
	    {type:"input", name:"cPatientName", 'placeholder': 'Name'},
	    {type: "selectItem", name: "cSimptoms"},
	    {type: "block", blockOffset: 0, list: [
	    	{type: "button", name: "btnCancel", value: "Cancel", className: 'btnCancel'},
		    {type: "newcolumn"},
		    {type: "button", name: "btnDone", value: "Done", className: 'btnDone', offsetLeft: 350}
	    ]}
	    
	];
	var myForm = myLayout.cells("b").attachForm();
	myForm.loadStruct(formStructure, "json");

	$(myForm.base).addClass('formBaseCss');
	$(myForm.base).parent().addClass('formCss');
	$(myForm.getInput('cPatientName')).attr("placeholder", "Name");
	

	// dhxForm = new dhtmlXForm("form_container", formStructure);
	myForm.attachEvent("onButtonClick", function(name){
		if (name == 'btnDone') {
			var cName = myForm.getItemValue('cPatientName');
			savePatient(cName, aChosenSimptoms);
			//SEND TO BACKEND cName + aChosenSimptoms
			//CHANGE SCREEN
		}
		if (name == 'btnCancel') {
			//CHANGE SCREENcu
		}

	});
}
