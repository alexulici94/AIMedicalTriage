var dataSimptoms = [
  {
    "_id": "5ab660757d26aa9f07300918",
    "name": "Bender Maynard"
  },
  {
    "_id": "5ab66075f964986241987e14",
    "name": "Branch Marshall"
  },
  {
    "_id": "5ab66075527790b22c6b032f",
    "name": "Horn Blevins"
  },
  {
    "_id": "5ab66075ae1dd629d5ca2982",
    "name": "Lee Gardner"
  },
  {
    "_id": "5ab66075631fcd9ed7afcc33",
    "name": "Noemi Hutchinson"
  },
  {
    "_id": "5ab66075766e0a76ea0749f1",
    "name": "Margo Klein"
  },
  {
    "_id": "5ab66075195147366b121809",
    "name": "Serena Golden"
  }
]; 

var dataCases = [
  {
    "_id": "5ab68f9dd47d4e6e7d5ea3af",
    "name": "Tammie Hopper",
    "score": 9,
    "diseases": [
      {
        "_idD": "5ab68f9d32df8a8863c5d49e",
        "name": "tammiehopper@anixang.com",
        "probability": 5.6906
      },
      {
        "_idD": "5ab68f9d1360f7ebdd49edff",
        "name": "tammiehopper@anixang.com",
        "probability": 0.9276
      },
      {
        "_idD": "5ab68f9d673d0e35aa3e94dd",
        "name": "tammiehopper@anixang.com",
        "probability": 35.565
      }
    ]
  },
  {
    "_id": "5ab68f9d7938783e21432a39",
    "name": "Jeannie Cleveland",
    "score": 10,
    "diseases": [
      {
        "_idD": "5ab68f9de97dc7211cc3d604",
        "name": "jeanniecleveland@anixang.com",
        "probability": 25.9914
      },
      {
        "_idD": "5ab68f9d27454d7925b99596",
        "name": "jeanniecleveland@anixang.com",
        "probability": 50.7313
      },
      {
        "_idD": "5ab68f9df142b313cfbb03e3",
        "name": "jeanniecleveland@anixang.com",
        "probability": 47.0039
      }
    ]
  },
  {
    "_id": "5ab68f9d631f23422a95aa27",
    "name": "Hart Rowland",
    "score": 7,
    "diseases": [
      {
        "_idD": "5ab68f9d903864c08b380aa2",
        "name": "hartrowland@anixang.com",
        "probability": 81.0465
      },
      {
        "_idD": "5ab68f9da33e98fc2442a1e7",
        "name": "hartrowland@anixang.com",
        "probability": 61.6308
      },
      {
        "_idD": "5ab68f9d72a15d3b0977c985",
        "name": "hartrowland@anixang.com",
        "probability": 1.8994
      }
    ]
  },
  {
    "_id": "5ab68f9d0c05d49f0a103ef5",
    "name": "Lynn Pittman",
    "score": 4,
    "diseases": [
      {
        "_idD": "5ab68f9dbf3b2d4808e3e387",
        "name": "lynnpittman@anixang.com",
        "probability": 2.3068
      },
      {
        "_idD": "5ab68f9dc78ca6bdae7ab253",
        "name": "lynnpittman@anixang.com",
        "probability": 98.7075
      },
      {
        "_idD": "5ab68f9d06f006c5f6f52812",
        "name": "lynnpittman@anixang.com",
        "probability": 75.9684
      }
    ]
  },
  {
    "_id": "5ab68f9d540bc9460488eec3",
    "name": "Watkins Gray",
    "score": 4,
    "diseases": [
      {
        "_idD": "5ab68f9d306ae26d81324610",
        "name": "watkinsgray@anixang.com",
        "probability": 55.8078
      },
      {
        "_idD": "5ab68f9d11d6f2c99c7c07f6",
        "name": "watkinsgray@anixang.com",
        "probability": 56.6204
      },
      {
        "_idD": "5ab68f9d00005f389749e301",
        "name": "watkinsgray@anixang.com",
        "probability": 80.4426
      }
    ]
  },
  {
    "_id": "5ab68f9dffd4692679fed6f8",
    "name": "Kinney Wise",
    "score": 6,
    "diseases": [
      {
        "_idD": "5ab68f9d72351a9e212c45b2",
        "name": "kinneywise@anixang.com",
        "probability": 20.9312
      },
      {
        "_idD": "5ab68f9d96cd425e41dafb3d",
        "name": "kinneywise@anixang.com",
        "probability": 60.7268
      },
      {
        "_idD": "5ab68f9de90a810714d10551",
        "name": "kinneywise@anixang.com",
        "probability": 71.796
      }
    ]
  }
]